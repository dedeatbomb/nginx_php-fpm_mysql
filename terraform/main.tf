provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}

resource "aws_key_pair" "ssh-key" {
        key_name        = "ssh-key"
        public_key      = var.aws_public-ssh-key
}

resource "aws_instance" "php-fpm_nginx" {
  ami                     = var.ami_latest
  instance_type           = var.instance_type
  vpc_security_group_ids  = [aws_security_group.php-fpm_nginx_security_group.id]
  key_name                = aws_key_pair.ssh-key.id
}

resource "aws_instance" "mysql" {
  ami                     = var.ami_latest
  instance_type           = var.instance_type
  vpc_security_group_ids  = [aws_security_group.mysql_security_group.id]
  key_name                = aws_key_pair.ssh-key.id
}

resource "null_resource" "ansible" {
  provisioner "local-exec" {
    command = "sleep 120; ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ubuntu --extra-var 'MYSQL_DB_NAME=${var.mysql_db_name} MYSQL_DB_USERNAME=${var.mysql_db_username} MYSQL_DB_PASSWORD=${var.mysql_db_password}' --private-key ${var.way-to-ssh-key_ansible} -i ${aws_instance.mysql.public_ip}, ../ansible/mysql/master.yml && ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ubuntu --private-key ${var.way-to-ssh-key_ansible} --extra-var 'MYSQL_HOST=${aws_instance.mysql.public_ip} MYSQL_DB_NAME=${var.mysql_db_name} MYSQL_DB_USERNAME=${var.mysql_db_username} MYSQL_DB_PASSWORD=${var.mysql_db_password}' -i ${aws_instance.php-fpm_nginx.public_ip}, ../ansible/php-fpm_nginx/master.yml"
  }
    depends_on = [
    aws_instance.mysql,
    aws_instance.php-fpm_nginx
  ]
}

